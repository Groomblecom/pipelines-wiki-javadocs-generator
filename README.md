#Pipelines Autogenerate Wiki From Javadocs
*I'm not good at naming things.*

These are a couple files to allow you to automatically put docs in your wiki from your source comments, using bitbucket pipelines. This is convenient because it takes ~30 seconds to set up, ensures that the documentation in the wiki is never behind the code, and lets you use easy-to-write javadoc-style source comments. 

##Usage
This script expects there to be source files with the extension .java (you can change it in the script) inside /src or recursively searched subdirectories of /src. It expects that there is no folder named /doc nor file named script.js or gitssh.sh. You will need to have enabled pipelines for the repo.
In order to authenticate, this tool uses a bitbucket app password, passed via a secured environmental variable. This prevents it being shown in the output, and allows you to limit the access of the script to wikis. Generating an app password is easy:

1.  Go to your bitbucket settings (icon in top right -> bitbucket settings), then select 'App passwords' in the left menu, then choose 'Create app password'. ![illustration 1](info1.png)
2.  Set the app password to *only* have access to wikis (this helps prevent accidents and provides a little extra security in case the app password does somehow leak), and create the app password. ![illustration 2](info3.png) 
    You will be given a small dialog with the app password. Copy it and put it in a temporary text file on your desktop. Before closing the page, make sure you copied it correctly.
3.  Paste the app password as an environment variable. You must set the name of the variable to `WIKI_APP_PASSWORD` or else it will not work. **You absolutely have to check 'Secured'.** Not checking secured could allow random people to edit your wikis- not good. Once you have made sure that the app password is correct, add the variable. ![illustration 3](info2.png)
4.  Copy and paste pipelines.yml and total-javadocs-to-wiki.sh from this repo to yours. If you already have a pipelines.yml, you can merge the two; just make sure the image you use has NodeJS, Git, and a standard bash environment. 
5.  Go to your wiki (enable it in project settings if it isn't there), and go to 'Clone Wiki' in the top right corner, select 'HTTPS' from the dropdown, and copy the resulting wiki repo slug. Go to total-javadocs-to-wiki.sh and replace the value on line 2 with the wiki repo slug you just copied, minus the 'git clone' part. Make sure it is in double quotes.
6.  Push your changes, and watch the magic happen. An index is generated in genlinks.md- you can navigate to it by going to the home page of your wiki, clicking on the name of your project (on the left, underneath 'Wiki'), and selecting genlinks from that list. For convenience, you may wish to add a link like `[Javadocs here!](genlinks.md)` to your home page.

Under the hood, this tool fetches and uses [Delight.im's tool for creating markdown from source comments](https://github.com/delight-im/Javadoc-to-Markdown) running in a docker image from [here](https://hub.docker.com/r/rowanto/docker-java8-mvn-nodejs-npm/~/dockerfile/). Many thanks to the respective authors for making their work available.
