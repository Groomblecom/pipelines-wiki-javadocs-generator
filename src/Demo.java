/**
 * A class with an important demonstration to make.
 * @author Groomblecom
 */
class Demo {
	/**
	 * Does something really complicated. 
	 * @param complicatedArgument A very complicated, documentation-worthy argument. Pay close attention.
	 * @return Zero, the most complicated integer.
	 */
	public int veryComplicatedFunction(Object complicatedArgument) {
		//Very important things.
		return 0;
	}
}
